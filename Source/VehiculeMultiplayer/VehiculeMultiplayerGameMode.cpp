// Copyright Epic Games, Inc. All Rights Reserved.

#include "VehiculeMultiplayerGameMode.h"
#include "VehiculeMultiplayerPawn.h"
#include "VehiculeMultiplayerHud.h"

AVehiculeMultiplayerGameMode::AVehiculeMultiplayerGameMode()
{
	DefaultPawnClass = AVehiculeMultiplayerPawn::StaticClass();
	HUDClass = AVehiculeMultiplayerHud::StaticClass();
}
