﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GoKartMovementComponent.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GoKartMovementReplicator.generated.h"

USTRUCT()
struct FGoKartState
{
	GENERATED_BODY()

	UPROPERTY()
	FTransform LastTransform;
	UPROPERTY()
	FVector Velocity;
	UPROPERTY()
	FGoKartMove LastMove;
};

USTRUCT()
struct FHermitCubicSpline
{
	GENERATED_BODY()

	FVector StartLocation, StartDerivative, TargetLocation, TargetDerivative;
	FQuat StartRotation, TargetRotation;
	
	FVector InterpolateLocation(const float LerpRatio) const
	{
		return FMath::CubicInterp(StartLocation, StartDerivative, TargetLocation, TargetDerivative, LerpRatio);
	}
	
	FVector InterpolateDerivative(const float LerpRatio) const
	{
		return FMath::CubicInterpDerivative(StartLocation, StartDerivative, TargetLocation, TargetDerivative, LerpRatio);
	}

	FQuat InterpolateRotation(const float LerpRatio) const
	{
		return FQuat::Slerp(StartRotation,TargetRotation, LerpRatio);
	}
};

USTRUCT()
struct FMyClientSimulatedMovement
{
	GENERATED_BODY()

	FTransform StartTransform, TargetTransform;
	FVector StartDerivative, TargetDerivative;
	float UpdateDuration;
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VEHICULEMULTIPLAYER_API UGoKartMovementReplicator final : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGoKartMovementReplicator();
	
protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	TArray<FGoKartMove> GetUnacknowledgedMoves() const;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

private:
	TArray<FGoKartMove> UnacknowledgedMoves;
	void ClearAcknowledgeMove(const FGoKartMove& LastMove);
	
	void UpdateServerState(const FGoKartMove& GoKartMove);
	float VelocityToDerivative() const;
	void CreateSpline(FHermitCubicSpline& Spline) const;
	void InterpolateSpline(const FHermitCubicSpline& Spline, float LerpRatio) const;
	void InterpolateVelocity(const FHermitCubicSpline& Spline, float LerpRatio) const;
	void Client_Tick(const float DeltaTime);

	UFUNCTION(Server, Reliable, WithValidation)
    void Server_SendMove(FGoKartMove GoKartMove);

	UPROPERTY(ReplicatedUsing=OnRep_GokartState)
	FGoKartState Server_GoKartState;
	UFUNCTION()
    void OnRep_GoKartState();

	void AutonomousProxy_OnRep_GoKartState();
	void SimulatedProxy_OnRep_GoKartState();

	float Client_TimeSinceUpdate;
	float Client_TimeBetweenUpdates;
	// FTransform Client_StartTransform;
	// FVector Client_StartDerivative;
	
	TArray<FMyClientSimulatedMovement> ClientSimulatedMovements;

	UPROPERTY()
	UGoKartMovementComponent* GoKartMovementComponent;
};
