﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GoKartMovementComponent.h"

#include "GameFramework/GameStateBase.h"
#include "Kismet/GameplayStatics.h"


// Sets default values for this component's properties
UGoKartMovementComponent::UGoKartMovementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UGoKartMovementComponent::BeginPlay()
{
	Super::BeginPlay();
	Ref_GameStateBase = UGameplayStatics::GetGameState(this);
}

void UGoKartMovementComponent::TickComponent(const float DeltaTime, const ELevelTick TickType,
                                             FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (GetOwnerRole() == ENetRole::ROLE_AutonomousProxy || Cast<APawn>(GetOwner())->IsLocallyControlled())
	{
		CreateGoKartMove(DeltaTime, LastMove, Ref_GameStateBase->GetServerWorldTimeSeconds());
		SimulateMovement(LastMove);
	}
}

void UGoKartMovementComponent::CreateGoKartMove(const float DeltaTime, FGoKartMove& GoKartMove, const float Timestamp) const
{
	GoKartMove.DeltaTime = DeltaTime;
	GoKartMove.GasPedalValue = GasPedalValue;
	GoKartMove.SteerValue = SteerValue;
	GoKartMove.Timestamp = Timestamp;
}

FVector UGoKartMovementComponent::GetResistance() const
{
	return -Velocity.GetSafeNormal() * Velocity.SizeSquared() * DragCoefficient;
}

FVector UGoKartMovementComponent::GetRollingResistance() const
{
	const float AccelerationDueToGravity = -GetWorld()->GetGravityZ() / 100;
	const float NormalForce = Mass * AccelerationDueToGravity;
	return -Velocity.GetSafeNormal() * NormalForce * RollingCoefficient;
}

FVector UGoKartMovementComponent::ApplyRotation(const float DeltaTime, const float NewSteerValue) const
{
	auto* TmpOwner = GetOwner();
	const float DeltaLocation = FVector::DotProduct(TmpOwner->GetActorForwardVector(), Velocity) * DeltaTime;
	const float RotationAngle = DeltaLocation / MinTurnRadius * NewSteerValue;
	const FQuat RotationDelta(TmpOwner->GetActorUpVector(), RotationAngle);
	const FRotator TmpRotator = TmpOwner->GetActorRotation() + RotationDelta.Rotator();
	TmpOwner->SetActorRotation(TmpRotator);

	return RotationDelta.RotateVector(Velocity);
}

void UGoKartMovementComponent::UpdateLocationFromVelocity(const float DeltaTime)
{
	FHitResult HitResult;
	GetOwner()->SetActorLocation(GetOwner()->GetActorLocation() + 100 * Velocity * DeltaTime, true, &HitResult);
	if (HitResult.IsValidBlockingHit())
	{
		Velocity = FVector::ZeroVector;
	}
}

void UGoKartMovementComponent::SimulateMovement(const FGoKartMove& GoKartMove)
{
	FVector Force = GetOwner()->GetActorForwardVector() * MaxDrivingForce * GoKartMove.GasPedalValue;
	Force += GetResistance();
	Force += GetRollingResistance();

	const FVector Acceleration = Force / Mass;
	Velocity += Acceleration * GoKartMove.DeltaTime;

	Velocity = ApplyRotation(GoKartMove.DeltaTime, GoKartMove.SteerValue);
	UpdateLocationFromVelocity(GoKartMove.DeltaTime);
}

FVector UGoKartMovementComponent::GetVelocity() const
{
	return Velocity;
}

float UGoKartMovementComponent::GetSteerValue() const
{
	return SteerValue;
}

void UGoKartMovementComponent::SetSteerValue(const float NewSteerValue)
{
	SteerValue = NewSteerValue;
}

float UGoKartMovementComponent::GetGasPedalValue() const
{
	return GasPedalValue;
}

void UGoKartMovementComponent::SetGasPedalValue(const float NewGasPedalValue)
{
	GasPedalValue = NewGasPedalValue;
}

FGoKartMove UGoKartMovementComponent::GetLastMove() const
{
	return LastMove;
}
