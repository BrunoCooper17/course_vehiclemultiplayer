// Copyright Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/HUD.h"
#include "VehiculeMultiplayerHud.generated.h"


UCLASS(config = Game)
class AVehiculeMultiplayerHud : public AHUD
{
	GENERATED_BODY()

public:
	AVehiculeMultiplayerHud();

	/** Font used to render the vehicle info */
	UPROPERTY()
	UFont* HUDFont;

	// Begin AHUD interface
	virtual void DrawHUD() override;
	// End AHUD interface
};
