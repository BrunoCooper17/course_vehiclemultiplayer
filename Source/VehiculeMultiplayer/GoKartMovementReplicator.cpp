﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "GoKartMovementReplicator.h"

#include "DrawDebugHelpers.h"
#include "Engine/DemoNetDriver.h"

DEFINE_LOG_CATEGORY_STATIC(LogGoKartMovementReplicator, All, All)

// Sets default values for this component's properties
UGoKartMovementReplicator::UGoKartMovementReplicator()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UGoKartMovementReplicator::BeginPlay()
{
	Super::BeginPlay();
	GoKartMovementComponent = GetOwner()->FindComponentByClass<UGoKartMovementComponent>();
}

void UGoKartMovementReplicator::TickComponent(const float DeltaTime, const ELevelTick TickType,
                                              FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GoKartMovementComponent == nullptr) return;

	const auto GoKartLastMove = GoKartMovementComponent->GetLastMove();

	// CLIENT (PAWN CONTROLLED BY A PLAYER PLAYING IN A CLIENT)
	if (GetOwnerRole() == ENetRole::ROLE_AutonomousProxy)
	{
		Server_SendMove(GoKartLastMove);
		UnacknowledgedMoves.Add(GoKartLastMove);

		GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Orange,
		                                 FString::Printf(TEXT("Queue Length: %d"), GetUnacknowledgedMoves().Num()));
	}
		// SERVER (PAWN CONTROLLED BY A PLAYER PLAYING IN THE SERVER)
	else if (GetOwner()->GetRemoteRole() == ROLE_SimulatedProxy)
	{
		UpdateServerState(GoKartLastMove);
	}
		// CLIENT (PAWN NOT CONTROLLED BY THE PLAYER PLAYING IN THIS CLIENT)
	else if (GetOwnerRole() == ENetRole::ROLE_SimulatedProxy)
	{
		Client_Tick(DeltaTime);
	}
}

TArray<FGoKartMove> UGoKartMovementReplicator::GetUnacknowledgedMoves() const
{
	return UnacknowledgedMoves;
}

void UGoKartMovementReplicator::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UGoKartMovementReplicator, Server_GoKartState);
}

void UGoKartMovementReplicator::ClearAcknowledgeMove(const FGoKartMove& LastMove)
{
	int32 Index = 1;
	for (const auto& TmpMove : UnacknowledgedMoves)
	{
		if (TmpMove.Timestamp >= LastMove.Timestamp)
		{
			break;
		}
		Index++;
	}

	Index = FMath::Clamp(Index, 0, UnacknowledgedMoves.Num());
	UnacknowledgedMoves.RemoveAt(0, Index, false);
}

void UGoKartMovementReplicator::UpdateServerState(const FGoKartMove& GoKartMove)
{
	if (GoKartMovementComponent == nullptr) return;

	Server_GoKartState.LastMove = GoKartMove;
	Server_GoKartState.LastTransform = GetOwner()->GetActorTransform();
	Server_GoKartState.Velocity = GoKartMovementComponent->GetVelocity();
}

float UGoKartMovementReplicator::VelocityToDerivative() const
{
	return Client_TimeBetweenUpdates * 100.0f;
}

void UGoKartMovementReplicator::CreateSpline(FHermitCubicSpline& Spline) const
{
	Spline.StartLocation = ClientSimulatedMovements[0].StartTransform.GetLocation();
	Spline.TargetLocation = ClientSimulatedMovements[0].TargetTransform.GetLocation();
	Spline.StartDerivative = ClientSimulatedMovements[0].StartDerivative * VelocityToDerivative();
	Spline.TargetDerivative = ClientSimulatedMovements[0].TargetDerivative * VelocityToDerivative();
	Spline.StartRotation = ClientSimulatedMovements[0].StartTransform.GetRotation();
	Spline.TargetRotation = ClientSimulatedMovements[0].TargetTransform.GetRotation();
}

void UGoKartMovementReplicator::InterpolateSpline(const FHermitCubicSpline& Spline, const float LerpRatio) const
{
	const FVector NextLocation = Spline.InterpolateLocation(LerpRatio);
	const FQuat NextRotation = Spline.InterpolateRotation(LerpRatio);
	GetOwner()->SetActorLocationAndRotation(NextLocation, NextRotation);
}

void UGoKartMovementReplicator::InterpolateVelocity(const FHermitCubicSpline& Spline, const float LerpRatio) const
{
	const FVector NewDerivative = Spline.InterpolateDerivative(LerpRatio);
	const FVector NewVelocity = NewDerivative / VelocityToDerivative();
	GoKartMovementComponent->SetVelocity(NewVelocity);
}

void UGoKartMovementReplicator::Client_Tick(const float DeltaTime)
{
	Client_TimeSinceUpdate += DeltaTime;

	// if (Client_TimeBetweenUpdates < KINDA_SMALL_NUMBER) return;
	if (ClientSimulatedMovements.Num() == 0) return;
	Client_TimeBetweenUpdates += DeltaTime;

	FHermitCubicSpline Spline;
	CreateSpline(Spline);

	const float LerpRatio = Client_TimeBetweenUpdates / ClientSimulatedMovements[0].UpdateDuration;
	InterpolateSpline(Spline, LerpRatio);
	InterpolateVelocity(Spline, LerpRatio);

	if (LerpRatio >= 1.f) // && ClientSimulatedMovements.Num() > 1)
	{
		Client_TimeBetweenUpdates -= ClientSimulatedMovements[0].UpdateDuration;
		ClientSimulatedMovements.RemoveAt(0, 1, false);
	}

	// for (auto& TmpMovement : ClientSimulatedMovements)
	// {
	// 	DrawDebugSphere(GetWorld(), TmpMovement.StartTransform.GetLocation(), 25, 12, FColor::Red, false, DeltaTime, 0, 15.0f);
	// 	DrawDebugSphere(GetWorld(), TmpMovement.TargetTransform.GetLocation(), 50, 12, FColor::Green, false, DeltaTime, 0, 15.0f);
	// }
	// GEngine->AddOnScreenDebugMessage(200, 2.0f, FColor::Cyan,
	//                                  FString::Printf(
	// 	                                 TEXT("%s Start: %s End: %s, LerpRatio: %f"), *GetFName().ToString(),
	// 	                                 *Spline.StartLocation.ToString(), *Spline.StartLocation.ToString(),
	// 	                                 LerpRatio));
	// UE_LOG(LogGoKartMovementReplicator, Log, TEXT("%s Start: %s End: %s, LerpRatio: %f"), *GetFName().ToString(),
	//        *Spline.StartLocation.ToString(), *Spline.StartLocation.ToString(), LerpRatio);
}

void UGoKartMovementReplicator::Server_SendMove_Implementation(const FGoKartMove GoKartMove)
{
	if (GoKartMovementComponent == nullptr) return;

	GoKartMovementComponent->SimulateMovement(GoKartMove);
	UpdateServerState(GoKartMove);
}

bool UGoKartMovementReplicator::Server_SendMove_Validate(FGoKartMove GoKartMove)
{
	return true;
}

void UGoKartMovementReplicator::OnRep_GoKartState()
{
	if (GoKartMovementComponent == nullptr) return;

	switch (GetOwnerRole())
	{
	case ROLE_None: break;

	case ROLE_SimulatedProxy:
		SimulatedProxy_OnRep_GoKartState();
		break;
	case ROLE_AutonomousProxy:
		AutonomousProxy_OnRep_GoKartState();
		break;

	case ROLE_Authority: break;
	case ROLE_MAX: break;
	default: ;
	}
}

void UGoKartMovementReplicator::AutonomousProxy_OnRep_GoKartState()
{
	if (GoKartMovementComponent == nullptr) return;

	GetOwner()->SetActorTransform(Server_GoKartState.LastTransform);
	GoKartMovementComponent->SetVelocity(Server_GoKartState.Velocity);

	ClearAcknowledgeMove(Server_GoKartState.LastMove);

	for (const auto& TmpMove : UnacknowledgedMoves)
	{
		GoKartMovementComponent->SimulateMovement(TmpMove);
	}
}

void UGoKartMovementReplicator::SimulatedProxy_OnRep_GoKartState()
{
	if (GoKartMovementComponent == nullptr) return;
	if (Client_TimeSinceUpdate < KINDA_SMALL_NUMBER) return;

	FMyClientSimulatedMovement NewClientSimulatedMovement;

	NewClientSimulatedMovement.StartDerivative = GoKartMovementComponent->GetVelocity();
	NewClientSimulatedMovement.TargetDerivative = Server_GoKartState.Velocity;
	// Client_StartDerivative = GoKartMovementComponent->GetVelocity();
	NewClientSimulatedMovement.StartTransform = ClientSimulatedMovements.Num() < 1
		                                            ? GetOwner()->GetActorTransform()
		                                            : ClientSimulatedMovements.Last().TargetTransform;
	NewClientSimulatedMovement.TargetTransform = Server_GoKartState.LastTransform;
	// Client_StartTransform = GetOwner()->GetActorTransform();
	NewClientSimulatedMovement.UpdateDuration = Client_TimeSinceUpdate;
	// Client_TimeBetweenUpdates = Client_TimeSinceUpdate;
	Client_TimeSinceUpdate = 0;

	ClientSimulatedMovements.Add(NewClientSimulatedMovement);

	// DrawDebugSphere(GetWorld(), Server_GoKartState.LastTransform.GetLocation(), 50, 12, FColor::Green, false,
	//                 Client_TimeBetweenUpdates, 0, 15.0f);
	// DrawDebugSphere(GetWorld(), Client_StartTransform.GetLocation(), 50, 12, FColor::Red, false,
	//                 Client_TimeBetweenUpdates, 0, 15.0f);
}
