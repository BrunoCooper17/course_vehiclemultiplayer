// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GoKartMovementComponent.h"
#include "GoKartMovementReplicator.h"

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GoKart.generated.h"

UCLASS()
class VEHICULEMULTIPLAYER_API AGoKart final : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AGoKart();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Movement")
	UGoKartMovementComponent* GoKartMovementComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Movement")
	UGoKartMovementReplicator* GoKartMovementReplicator;
	
	UFUNCTION()
    void MoveForward(float Value);
	UFUNCTION()
    void MoveRight(float Value);

private:
	static FString GetStringRole(ENetRole NetRole);
};
