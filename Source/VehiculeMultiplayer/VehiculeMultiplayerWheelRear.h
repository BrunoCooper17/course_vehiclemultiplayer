// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "VehiculeMultiplayerWheelRear.generated.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS

UCLASS()
class UVehiculeMultiplayerWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UVehiculeMultiplayerWheelRear();
};

PRAGMA_ENABLE_DEPRECATION_WARNINGS

