// Copyright Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "VehiculeMultiplayerGameMode.generated.h"

UCLASS(minimalapi)
class AVehiculeMultiplayerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AVehiculeMultiplayerGameMode();
};



