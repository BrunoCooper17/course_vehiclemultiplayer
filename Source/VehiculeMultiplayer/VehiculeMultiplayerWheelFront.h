// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "VehiculeMultiplayerWheelFront.generated.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS

UCLASS()
class UVehiculeMultiplayerWheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UVehiculeMultiplayerWheelFront();
};

PRAGMA_ENABLE_DEPRECATION_WARNINGS


