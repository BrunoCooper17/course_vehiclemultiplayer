﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GoKartMovementComponent.generated.h"

USTRUCT()
struct FGoKartMove
{
	GENERATED_BODY()

	UPROPERTY()
	float SteerValue;
	UPROPERTY()
	float GasPedalValue;

	UPROPERTY()
	float DeltaTime;
	UPROPERTY()
	float Timestamp;
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VEHICULEMULTIPLAYER_API UGoKartMovementComponent final : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGoKartMovementComponent();

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	void CreateGoKartMove(float DeltaTime, FGoKartMove& GoKartMove, const float Timestamp) const;
	FVector GetResistance() const;
	FVector GetRollingResistance() const;
	FVector ApplyRotation(const float DeltaTime, const float NewSteerValue) const;
	void UpdateLocationFromVelocity(float DeltaTime);

public:
	void SimulateMovement(const FGoKartMove& GoKartMove);
	
	void SetVelocity(const FVector NewVelocity) { Velocity = NewVelocity; }
	FVector GetVelocity() const;

	float GetSteerValue() const;
	void SetSteerValue(const float NewSteerValue);
	float GetGasPedalValue() const;
	void SetGasPedalValue(const float NewGasPedalValue);
	FGoKartMove GetLastMove() const;

private:
	// The force applied to the car when the GasPedal is fully down (N)
	UPROPERTY(EditAnywhere)
	float MaxDrivingForce = 10000;

	// Minimum turning circle radius at full lock (Meters)
	UPROPERTY(EditAnywhere)
	float MinTurnRadius = 10;

	// Air Resistance (Drag). Higher means more drag 
	UPROPERTY(EditAnywhere)
	float DragCoefficient = 16;

	// Higher means more Rolling resistance 
	UPROPERTY(EditAnywhere)
	float RollingCoefficient = 0.015f;

	// The mass of the car (KG)
	UPROPERTY(EditAnywhere)
	float Mass = 1000;

	FGoKartMove LastMove;
	
private:
	FVector Velocity;
	float SteerValue;
	float GasPedalValue;

	UPROPERTY()
	class AGameStateBase* Ref_GameStateBase;
};
