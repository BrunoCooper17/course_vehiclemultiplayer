// Fill out your copyright notice in the Description page of Project Settings.


#include "GoKart.h"
#include "DrawDebugHelpers.h"

DEFINE_LOG_CATEGORY_STATIC(LogGoKart, All, All)

// Sets default values
AGoKart::AGoKart()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	SetReplicateMovement(false);

	GoKartMovementComponent = CreateDefaultSubobject<UGoKartMovementComponent>(FName("MovementComponent"));
	GoKartMovementReplicator = CreateDefaultSubobject<UGoKartMovementReplicator>(FName("MovementReplicator"));

	if (GoKartMovementReplicator)
	{
		GoKartMovementReplicator->SetIsReplicated(true);
	}
}

// Called when the game starts or when spawned
void AGoKart::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		NetUpdateFrequency = 10;
	}
}

// Called to bind functionality to input
void AGoKart::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(FName("MoveForward"), this, &AGoKart::MoveForward);
	PlayerInputComponent->BindAxis(FName("MoveRight"), this, &AGoKart::MoveRight);
}

// Called every frame
void AGoKart::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);

	// DrawDebugString(GetWorld(), FVector::UpVector * 20,
	//                 FString::Printf(TEXT("Vel %0.2f"), GoKartMovementComponent->GetVelocity().Size()), this, FColor::White, DeltaTime);
	DrawDebugString(GetWorld(), FVector::ZeroVector, GetStringRole(GetLocalRole()), this, FColor::White, DeltaTime);
}

void AGoKart::MoveForward(const float Value)
{
	if (GoKartMovementComponent == nullptr) return;
	GoKartMovementComponent->SetGasPedalValue(FMath::Clamp(Value, -1.0f, 1.0f));
}

void AGoKart::MoveRight(const float Value)
{
	if (GoKartMovementComponent == nullptr) return;
	GoKartMovementComponent->SetSteerValue(FMath::Clamp(Value, -1.0f, 1.0f));
}

FString AGoKart::GetStringRole(const ENetRole NetRole)
{
	switch (NetRole)
	{
	case ROLE_None:
		return FString("None :(");
	case ROLE_SimulatedProxy:
		return FString("SimulatedProxy");
	case ROLE_AutonomousProxy:
		return FString("AutonomousProxy");
	case ROLE_Authority:
		return FString("Authority");
	case ROLE_MAX:
		return FString("Authority?");
	default:
		return FString("Unknown :(");;
	}
}
